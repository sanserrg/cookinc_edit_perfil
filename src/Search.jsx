import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Controller from './AppController'


const Search = () => {

    const [busqueda, setBusqueda] = useState("");
    const [resultados, setResultados] = useState([]);
    const [filas, setFilas] = useState([]);

    useEffect(() => {
        Controller.getAllRecetas()
            .then(datos => setResultados(datos))
            .catch(err => console.log(err))
    }, [])

    useEffect(() => {
        if (busqueda !== "") {
            const filasAux = resultados.map((el) => {
                if (el.nombre.toUpperCase().startsWith(busqueda.toUpperCase())) {
                    return (
                        <Link to={"/receta/" + el._id} className="dropdown-item">
                            {el.nombre}
                        </Link>
                    );
                }
            });
            setFilas(filasAux);
        } else {
            setFilas("");
        }
    }, [busqueda])

    return (
        <>
            <div className="search">
                <i id="search-icon" className="fas fa-search mr-2"></i>
                <input id="search-bar" type="text" value={busqueda} onChange={(e) => setBusqueda(e.target.value)} />
                {busqueda &&
                    <div className="search-content">
                        {filas}
                    </div>
                }
            </div>
        </>
    );
}

export default Search;