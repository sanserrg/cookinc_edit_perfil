import { useEffect, useState } from 'react';
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'reactstrap';
import './App.css';

import NavBar from './NavBar';
import Controller from './AppController';
import Home from './Home';
import Perfil from './Perfil';
import NuevaReceta from "./NuevaReceta";
import Receta from './Receta';
import NotFound from './NotFound';
import Aside from './AsideBar';
import EditPerfil from './EditPerfil';

function App() {

  const [idUser, setIdUser] = useState('');
  const [nameUser, setNameUser] = useState('');
  const [loggeado, setLoggeado] = useState(false);

  const[searchSelect, setSearchSelect] = useState('')

  //OBTENER ID LOCALSTORAGE (SI HAY ALGUIEN REGISTRADO)
  useEffect(() => {
    let localUser = Controller.getLocalUserId()
    if (localUser) {
      setIdUser(localUser.id)
      setNameUser(localUser.nombre)
      setLoggeado(true)
    }
  }, [loggeado])



  return (
    <div className="App">
      <BrowserRouter>
      
        <NavBar user={nameUser} id={idUser} log={loggeado} setlog={setLoggeado} />
        <Aside />
        <div className="spacer"></div>
        
        <Container>
          <div className="content">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/nuevo" render={() => <NuevaReceta id={idUser} user={nameUser} log={loggeado} />} />
              <Route path="/receta/:id" component={Receta} />
              <Route exact path="/:user" render={() => <Perfil user={nameUser} log={loggeado} />} />
              <Route exact path="/:id/edit" render={() => <EditPerfil id={idUser} user={nameUser} log={loggeado} />} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Container>
      </BrowserRouter>
    </div>
  );
}

export default App;
