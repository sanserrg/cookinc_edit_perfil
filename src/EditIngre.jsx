import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input } from 'reactstrap';

const EditIngre = (props) => {

    const currentIngre = props.array[props.idx]

    const [cantidad, setCantidad] = useState(currentIngre.cantidad);
    const [unidad, setUnidad] = useState(currentIngre.unidad)
    const [nameIng, setNameIng] = useState(currentIngre.nombre);

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const ingredient = {
        cantidad,
        unidad,
        nombre: nameIng
    }

    const changeIng = () => {

        const newArr = props.array.map((el, idx) =>{
            if(idx === props.idx){
                el = ingredient;
                return el;
            }
            return el;
        })

        props.setarray([...newArr]);

        setModal(!modal);
    }

    return (
        <div>
            <Button className="btn btn-success btn-sm" onClick={toggle}> MODIFICAR </Button>
            <Modal isOpen={modal} toggle={toggle} >
                <ModalHeader toggle={toggle}>NUEVO PASO</ModalHeader>
                <ModalBody>

                <FormGroup>
                        <Label for="cantidad">Cantidad</Label>
                        <Input type="number" name="cantidad" id="cantidad" value={cantidad} onChange={(e) => setCantidad(e.target.value)} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="unidad">Unidad</Label>
                        <Input type="text" name="unidad" id="unidad" value={unidad} onChange={(e) => setUnidad(e.target.value)} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="name-ingre">Nombre Ingrediente</Label>
                        <Input type="text" name="name-ingre" id="name-ingre" value={nameIng} onChange={(e) => setNameIng(e.target.value)} />
                    </FormGroup>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={changeIng}>Guardar</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancelar</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default EditIngre;