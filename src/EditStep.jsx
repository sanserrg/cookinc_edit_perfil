import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input, Container } from 'reactstrap';

const EditStep = (props) => {

    const currentStep = props.array[props.idx];

    const [paso, setPaso] = useState(currentStep.paso);

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const editPaso = { paso }

    const changeStep = () => {

        const newArr = props.array.map((el, idx) => {
            if (idx === props.idx) {
                el = editPaso;
                return el;
            }
            return el;
        })

        props.setarray([...newArr]);

        setModal(!modal);
    }

    return (
        <div>
            <Button className="btn btn-success btn-sm" onClick={toggle}> MODIFICAR </Button>
            <Modal isOpen={modal} toggle={toggle} >
                <ModalHeader toggle={toggle}>NUEVO PASO</ModalHeader>
                <ModalBody>

                    <FormGroup >
                        <Container>
                            <Label for="paso">Describe detalladamente el paso</Label>
                            <Input type="textarea" name="paso" id="paso" value={paso} onChange={(e) => setPaso(e.target.value)} />
                        </Container>
                    </FormGroup>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={changeStep}>Guardar</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancelar</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default EditStep;