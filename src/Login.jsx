import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';

import Controller from './AppController'

const Login = (props) => {

    /* STATES RECOGEN DATOS A BUSCAR */
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [alert, setAlert] = useState('');

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);


    const openLog = () => {
        setModal(!modal);
        setAlert('');
    }

    const usuario = { email, password }


    const buscaUser = () => {
        Controller.searchUser(usuario)
            .then(user => {
                if (user !== null) {
                    Controller.saveUserLocal({ id: user[0]._id, nombre: user[0].nombre });
                    props.setlog(true)
                    setModal(!modal);
                    setEmail('')
                    setPassword('')
                }
            })
            .catch(err => {
                setAlert("Usuario o contraseña incorrecto");
            })
    }

    return (
        <div>
            <div className="nav-btn" onClick={openLog}>
                <span className="nav-icon">
                    <i className="fas fa-door-open"></i>
                </span>
                <h5>Inicia sesión</h5>
            </div>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Identificate</ModalHeader>
                {alert && <Alert color="danger">{alert}</Alert>}
                <ModalBody>
                    <Form>
                        <FormGroup >
                            <Label for="email" >Email</Label>
                            <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                        </FormGroup>
                        <FormGroup >
                            <Label for="password" >Password</Label>
                            <Input type="password" name="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="info" onClick={buscaUser}>OK</Button>{' '}
                    <Button color="secondary" onClick={toggle}>CANCELAR</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default Login;