import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input, Container } from 'reactstrap';

const NuevoPaso = (props) => {

    const [paso, setPaso] = useState('');

    const [modal, setModal] = useState(false);

    const toggle = () => {
        setModal(!modal);
        setPaso('')
    }

    const nuevoPaso = { paso }

    const saveIng = () => {
        props.setarray([...props.array, nuevoPaso]);
        setModal(!modal);
    }

    return (
        <div>
            <Button color="primary" onClick={toggle} className="btn-sm"> AÑADIR PASO </Button>
            <Modal isOpen={modal} toggle={toggle} >
                <ModalHeader toggle={toggle}>NUEVO PASO</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Container>
                            <Label for="paso">Describe detalladamente el paso</Label>
                            <Input type="textarea" name="paso" id="paso" value={paso} onChange={(e) => setPaso(e.target.value)}/>
                        </Container>
                    </FormGroup>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={saveIng}>Guardar</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancelar</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default NuevoPaso;