import React, { useState } from 'react';

import { Link } from 'react-router-dom';

import {
    Col,
    NavbarBrand,
    NavItem,
} from 'reactstrap';

import Login from './Login';
import Registro from './Registro';
import Search from './Search'
import Controller from './AppController';

const NavBar = (props) => {


    //EDITAR PERFIL



    // LOGOUT
    const logout = () => {
        Controller.deleteUserId(props.user);
        props.setlog(false);
    }

    return (
        <>
            <nav className="row navigation m-0 p-0" light expand="md">

                <div className="nav-sep">
                    <div className="sep-bg sep1"></div>
                    <div className="sep-bg sep2"></div>
                </div>
                <div className="nav-sep-2">
                    <div className="sep-bg sep1"></div>
                </div>
                <div className="nav-right"></div>

                <Col md="3 d-flex">
                    <NavbarBrand href="/">
                        <div className="nav-btn">
                            <span id="logo">
                                <i className="fas fa-utensils"></i>
                            </span>
                            <h1>Cook Inc</h1>
                        </div>
                    </NavbarBrand>
                </Col>

                <Col md="5">
                    <Search setsel={props.setsel} />
                </Col>

                <Col md="4 p-0">
                    <div className="right-content">
                        {props.log &&
                            <>
                                <NavItem>
                                    <Link to="/nuevo/" className="nav-btn">
                                        <div className="new-bg">
                                            <span className="nav-icon">
                                                <i className="fas fa-plus-circle"></i>
                                            </span>
                                        </div>
                                        <h5>Nuevo</h5>
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <div class="dropdown">
                                        <div className="nav-btn">
                                            <h5>{props.user.toUpperCase()}</h5>
                                            <span className="nav-icon">
                                                <i className="fas fa-user-circle"></i>
                                            </span>
                                            <div className="dropdown-content">
                                                <Link to={"/" + props.user} className="dropdown-item" >
                                                    <i class="fas fa-id-badge"></i>
                                                    <p>Ver perfil</p>
                                                </Link>
                                                <Link to={"/" + props.id + "/edit"} className="dropdown-item">
                                                    <i class="fas fa-cogs"></i>
                                                    <p>Editar</p>
                                                </Link>
                                                <div className="dropdown-item">
                                                    <i className="fas fa-sign-out-alt" onClick={logout}></i>
                                                    <p>Cerrar sesión</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </NavItem>
                            </>
                        }

                        {!props.log &&
                            <>
                                <NavItem>
                                    <Registro />
                                </NavItem>
                                <NavItem>
                                    <Login setlog={props.setlog} />
                                </NavItem>
                            </>
                        }
                    </div>
                </Col>
            </nav>
        </>
    )
}

export default NavBar;