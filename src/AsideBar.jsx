import React, { useState, useEffect } from "react";
import styled from 'styled-components'

const Aside = () => {

    // const [asideWidth, setAsideWidth] = useState(90);
    // const [openAside, setOpenAside] = useState(false);

    // useEffect(() => {
    //     (openAside) ? setAsideWidth(400) : setAsideWidth(90);
    // }, [openAside])

    // const AsideBg = styled.div`
    //     width: ${asideWidth}px;
    //     height: 100%;
    //     position: fixed;
    //     left: 0;
    //     background-color: #DED8FF;
    //     padding-top: 80px;
    // `

    // const clickBoton = () => setOpenAside(!openAside);

    return (
        <>
            {/* <AsideBg> */}
                <div className="aside">
                    <div className="aside-btn">
                        <div className="aside-icon-bg">
                            <span className="nav-icon">
                                <i className="fab fa-delicious"></i>
                            </span>
                        </div>
                        <p>Filtrar</p>
                    </div>
                    <div className="aside-btn">
                        <div className="aside-icon-bg">
                            <span className="nav-icon">
                                <i className="fas fa-snowflake"></i>
                            </span>
                        </div>
                        <p>Nevera</p>
                    </div>
                    <div className="aside-btn">
                        <div className="aside-icon-bg">
                            <span className="nav-icon">
                                <i className="fas fa-tasks"></i>
                            </span>
                        </div>
                        <p>Compra</p>
                    </div>
                </div>
            {/* </AsideBg> */}
        </>
    );
}

export default Aside;