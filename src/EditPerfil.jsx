import React, { useState, useEffect } from 'react';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import Controller from './AppController';


const EditPerfil = (props) => {

    const [nombre, setNombre] = useState("");
    const [email, setEmail] = useState("");
    const [pass, setPass] = useState("");
    const [newPass, setNewPass] = useState("");
    const [newPass2, setNewPass2] = useState("");
    const [incomplete, setIncomplete] = useState(false); // falta modal => datos incompletos

    useEffect(() => {
        Controller.getUsuarioById(props.id)
            .then(data => {
                setNombre(data.nombre)
                setEmail(data.email)
                setPass(data.password)
            })
            .catch(err => console.log(err))
    }, [])

    const verificar = (pass1, pass2) => {

        const newProfile = {
            nombre,
            email,
            password: newPass
        }
        if ((pass1 !== "" && pass2 === "") || (pass1 === "" && pass2 !== "")) {
            setIncomplete(true)
        } else if (pass1 !== "" && (pass1 === pass2)) {
            setIncomplete(false);
            editProfile(newProfile, props.id);
        }
    }



    const editProfile = (profile, id) => {
        Controller.editUser(profile, id)
    }

    return (
        <div>
            <h1>ESTA ES LA PÁGINA DE EDITAR PERFIL DE {props.user.toUpperCase()}</h1>
            <FormGroup>
                <Label>Nombre</Label>
                <Input type="text" value={nombre} onChange={(e) => setNombre(e.target.value)} />
            </FormGroup>
            <FormGroup>
                <Label>Correo electrónico</Label>
                <Input type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
            </FormGroup>
            <FormGroup>
                <Label>Contraseña</Label>
                <Input disabled type="password" value={pass} />
            </FormGroup>
            <FormGroup>
                <Label>Contraseña nueva</Label>
                <Input type="password" value={newPass} onChange={(e) => setNewPass(e.target.value)} />
            </FormGroup>
            <FormGroup>
                <Label>Repite la contraseña</Label>
                <Input type="password"value={newPass2}  onChange={(e) => setNewPass2(e.target.value)} />
            </FormGroup>
            <Button color="primary" className="btn-sm" onClick={() => verificar(newPass, newPass2)}>GUARDAR</Button>
        </div>
    );
}

export default EditPerfil;