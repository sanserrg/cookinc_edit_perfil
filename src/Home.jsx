import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardTitle, CardText, CardFooter } from 'reactstrap';
import Controller from './AppController';
import CartaReceta from './CardReceta';

const Home = () => {

    const [allRecetas, setAllRecetas] = useState([]);

    useEffect(() => {
        Controller.getAllRecetas()
            .then(data => setAllRecetas(data))
            .catch(err => console.log(err))
    }, [])

    const cartasRecetas = allRecetas.map((el, idx) => {

        el.id = el._id;
        return (
            <CartaReceta key={idx} receta={el}>

            </CartaReceta>
        )
    })


    return (
        <div>
            {cartasRecetas}
        </div>
    );
}

export default Home;