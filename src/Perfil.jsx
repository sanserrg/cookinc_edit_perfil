import React from 'react';
import { Link, Redirect } from 'react-router-dom';


const Perfil = (props) => {

    if(!props.log) return <Redirect to="/"/>

    return (
        <div>
            <h1 className="m-5">ESTO ES EL PERFIL DE {props.user.toUpperCase()}</h1>
            <Link to="/nuevo" className="btn btn-info m-5">CREAR RECETA</Link>
        </div>
    );
}

export default Perfil;