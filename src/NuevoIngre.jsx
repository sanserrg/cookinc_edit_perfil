import React, { useState } from 'react';
import { Button, FormGroup, Label, Input } from 'reactstrap';

const NewIngre = (props) => {

    const [cantidad, setCantidad] = useState('');
    const [unidad, setUnidad] = useState('')
    const [nameIng, setNameIng] = useState('');



    const ingredient = {
        cantidad,
        unidad,
        nombre: nameIng
    }

    /* AÑADIR FILA EN LA TABLA */

    const saveIng = () => {
        props.setarray([...props.array, ingredient]);
        setCantidad('');
        setUnidad('');
        setNameIng('');
    }

    return (
        <>
            <FormGroup>
                <Label for="quantity">Cantidad</Label>
                <Input type="number" name="quantity" id="quantity" value={cantidad} onChange={(e) => setCantidad(e.target.value)} />
            </FormGroup>
            <FormGroup>
                <Label for="unity">Unidad</Label>
                <Input type="text" name="unity" id="unity" value={unidad} onChange={(e) => setUnidad(e.target.value)} />
            </FormGroup>
            <FormGroup>
                <Label for="name-ingre">Nombre Ingrediente</Label>
                <Input type="text" name="name-ingre" id="name-ingre" value={nameIng} onChange={(e) => setNameIng(e.target.value)} />
            </FormGroup>
            <Button color="primary" onClick={saveIng} className="btn-sm">AÑADIR INGREDIENTE</Button>
        </>
    );
}

export default NewIngre;