import React from 'react';

const NotFound = () => <h3>Ruta inexistent!</h3>

export default NotFound;